package com.bofugames.psy.services.business.srv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bofugames.psy.services.business.dto.User;
import com.bofugames.psy.services.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepo;
	
	public User addU(User usuario) {
		return userRepo.save(usuario);
	}
	
	public int validarCorreo(String email) {
		return userRepo.validarCorreo(email);
	} 
}
