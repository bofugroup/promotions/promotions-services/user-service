package com.bofugames.psy.services.facade.mapper.Imp;

import org.springframework.stereotype.Component;

import com.bofugames.psy.services.business.dto.User;
import com.bofugames.psy.services.facade.dto.DTOUsuarioRequest;
import com.bofugames.psy.services.facade.dto.DTOUsuarioResponse;
import com.bofugames.psy.services.facade.mapper.IUserMapper;

@Component("userMapper")
public class UserMapper implements IUserMapper
{

	@Override
	public DTOUsuarioResponse mapOutUser(User user) {
		DTOUsuarioResponse response = new DTOUsuarioResponse();
		if(user != null)
		{
			response.setFristName(user.getFristName());
			response.setLastName(user.getLastName());
			response.setEmail(user.getEmail());
			response.setDateBirth(user.getDateBirth());
			response.setMiddleName(user.getMiddleName());
			response.setPassword(user.getPassword());
		}
		return response;
	}

	@Override
	public User mapInUser(DTOUsuarioRequest request) 
	{
		User user = new User();
		if(request != null)
		{
			user.setMiddleName(request.getMiddleName());
			user.setFristName(request.getFristName());
			user.setLastName(request.getLastName());
			user.setEmail(request.getEmail());
			user.setPassword(request.getPassword());
			user.setDateBirth(request.getDateBirth());
		}
	
		return user;
	}

}
