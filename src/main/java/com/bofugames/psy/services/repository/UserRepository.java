package com.bofugames.psy.services.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.bofugames.psy.services.business.dto.User;


public interface UserRepository extends CrudRepository<User, Long> {
	@Query(value = "SELECT count(correo) FROM usuario_tbl"
			+ " WHERE correo=:email", nativeQuery = true)
	public int validarCorreo(@Param("email")String email);

}
