package com.bofugames.psy.services.facade.dto;

public class DTOUsuarioResponse 
{
	Long id;
	String fristName;
	String middleName;
	String lastName;
	String dateBirth;
	String email;
	String password;
	


	public DTOUsuarioResponse() {
		super();
	}

	public DTOUsuarioResponse(Long id, String fristName, String middleName, String lastName, String dateBirth,
			String email, String password) {
		super();
		this.id = id;
		this.fristName = fristName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.dateBirth = dateBirth;
		this.email = email;
		this.password = password;
	}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	@Override
	public String toString() {
		return "DTOUsuarioResponse [id=" + id + ", fristName=" + fristName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", dateBirth=" + dateBirth + ", email=" + email + ", password=" + password
				+ "]";
	}

}
