package com.bofugames.psy.services.business.dto;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usuario_tbl")
public class User implements Serializable{
	

	private static final long serialVersionUID = 4894729030347835498L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	@Column(name="nombre",nullable=false)
	String fristName;
	@Column(name="nombredos",nullable=false)
	String middleName;
	@Column(name="apellido",nullable=false)
	String lastName;
	@Column(name="fechaNa",nullable=false)
	String dateBirth;
	@Column(name="correo",nullable=false)
	String email;
	@Column(name="contrasena",nullable=false)
	String password;
	
	public User(String fristName, String middleName, String lastName, String dateBirth, String email,
			String password) {
		super();
		this.fristName = fristName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.dateBirth = dateBirth;
		this.email = email;
		this.password = password;
	}
	
	public User() {}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", fristName=" + fristName + ", middleName=" + middleName + ", lastName=" + lastName
				+ ", dateBirth=" + dateBirth + ", email=" + email + ", password=" + password + "]";
	}
	
}
