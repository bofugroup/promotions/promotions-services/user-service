package com.bofugames.psy.services.facade.mapper;

import com.bofugames.psy.services.business.dto.User;
import com.bofugames.psy.services.facade.dto.DTOUsuarioRequest;
import com.bofugames.psy.services.facade.dto.DTOUsuarioResponse;

public interface IUserMapper 
{
	DTOUsuarioResponse mapOutUser(User user);
	User mapInUser(DTOUsuarioRequest response);
}
