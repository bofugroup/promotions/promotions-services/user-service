package com.bofugames.psy.services.facade.controller.Imp;

import org.apache.logging.log4j.Logger;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.bofugames.psy.services.business.dto.User;
import com.bofugames.psy.services.business.srv.UserService;
import com.bofugames.psy.services.facade.controller.IUserController;
import com.bofugames.psy.services.facade.dto.DTOUsuarioRequest;
import com.bofugames.psy.services.facade.dto.DTOUsuarioResponse;
import com.bofugames.psy.services.facade.mapper.IUserMapper;
import com.bofugames.psy.services.util.ExceptionsUser;

@RestController
@RequestMapping(value="/users/v00/users")
public class UserController implements IUserController
{
	private static Logger logger = LogManager.getLogger(UserController.class);
	
	@Autowired
	UserService uService;

	
	@Autowired
	IUserMapper mapper;
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<String> DTOusuarioResponsesave (@RequestBody @Valid DTOUsuarioRequest dtousuarioRequest) throws ExceptionsUser {

		logger.info("se inicio la creacion del uusuario" + dtousuarioRequest.toString());
		
		String email;
		
		HttpHeaders response = new HttpHeaders();
		User usuario = mapper.mapInUser(dtousuarioRequest);
		if(uService.validarCorreo(usuario.getEmail())!=0 )throw new ExceptionsUser(); 
	
			User updatedUsuario = uService.addU(usuario);
			DTOUsuarioResponse dtousuarioResponse = mapper.mapOutUser(updatedUsuario);
			return new ResponseEntity<>("Usuario Agregado",response, HttpStatus.OK);

	}
	
	/*public DTOUsuarioResponse save(@RequestBody DTOUsuarioRequest dtousuarioRequest) 
	{
		logger.info("se inicio la creacion del uusuario" + dtousuarioRequest.toString());
		
		User usuario = mapper.mapInUser(dtousuarioRequest);
		User updatedUsuario = uService.addU(usuario);

		DTOUsuarioResponse dtousuarioResponse = mapper.mapOutUser(updatedUsuario);
		
		return dtousuarioResponse;
	}*/

	@Override
	public DTOUsuarioResponse save(DTOUsuarioRequest dtousuarioRequest) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

