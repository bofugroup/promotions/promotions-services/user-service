package com.bofugames.psy.services.facade.controller;

import com.bofugames.psy.services.facade.dto.DTOUsuarioRequest;
import com.bofugames.psy.services.facade.dto.DTOUsuarioResponse;

public interface IUserController
{
	DTOUsuarioResponse save(DTOUsuarioRequest dtousuarioRequest);
}
