package com.bofugames.psy.services.facade.dto;


import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DTOUsuarioRequest {

	Long id;
	@NotNull(message="El primer nombre es requerido")
	@Size(min=2, max=30, message="El nombre debe tener entre {min} y {max} caracteres")
	String fristName;
	@NotNull(message="El segundo nombre es requerido")
	@Size(min=2, max=30, message="El nombre debe tener entre {min} y {max} caracteres")
	String middleName;
	@NotNull(message="El apellido es requerido")
	@Size(min=2, max=30, message="El nombre debe tener entre {min} y {max} caracteres")
	String lastName;
	@NotNull(message="Fecha de nacimiento es requerida")
	@Size(min=2, max=30, message="El nombre debe tener entre {min} y {max} caracteres")
	String dateBirth;
	@NotNull(message="Correo requerido")
	@Email
	@NotBlank
	@Column(unique=true)
	String email;
	@NotNull(message="La contraseña es requerida")
	@Size(min=2, max=30, message="La contraseña debe tener entre {min} y {max} caracteres")
	String password;
	
	public DTOUsuarioRequest() {
		super();
	}

	public DTOUsuarioRequest(Long id, String fristName, String middleName, String lastName, String dateBirth,
			String email, String password) {
		super();
		this.id = id;
		this.fristName = fristName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.dateBirth = dateBirth;
		this.email = email;
		this.password = password;
	}


	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "DTOUsuarioRequest [id=" + id + ", fristName=" + fristName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", dateBirth=" + dateBirth + ", email=" + email + ", password=" + password + "]";
	}
	
}
